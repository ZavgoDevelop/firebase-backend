// Helper Functions

export const omit = (data: IOmit): ObjectData => {
  const { obj, props } = data;
  const newObj = { ...obj };

  props.forEach((prop) => {
    delete newObj[prop];
  });

  return newObj;
};

export const validateEmail = (data: IValidateEmail): boolean => {
  return EXPRESSION.test(String(data.email).toLowerCase());
};

export const getFormData = (data: ObjectData): FormData => {
  const formData = new FormData();

  for (const key in data) {
    if (data.hasOwnProperty(key)) {
      formData.append(key, data[key]);
    }
  }

  return formData;
}

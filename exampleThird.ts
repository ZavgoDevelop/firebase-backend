export class UrlBuilder {
  baseUrl: string;
  params: Map<string, string>;

  constructor() {
    this.baseUrl = "";
    this.params = new Map<string, string>();
  }

  public setBaseUrl(url: string) {
    this.baseUrl = url;

    return this;
  }

  public addParams(key: string, value: any) {
    this.params.set(key, value);

    return this;
  }

  public buildUrl() {
    const queryString = this.buildQueryString();

    return `${this.baseUrl}${queryString}`;
  }

  private buildQueryString() {
    if (!this.params.size) {
      return "";
    }

    const queryParams = [];

    for (const [key, value] of this.params) {
      if (value) {
        queryParams.push(`${key}=${value}`);
      }
    }

    return `?${queryParams.join("&")}`;
  }
}

// Sample code

import admin from "firebase-admin";

import db from "";
import * as dashboard from "";
import * as userHelper from "";
import * as foursquare from "";
import * as venueHelper from "";
import * as claimVenueHelper from "";

import { IClaimVenue } from "../../interfaces/venues";

export class ClaimVenueController {
  static async claimVenue(data: IClaimVenue) {
    const {
      role,
      extId,
      email,
      phone,
      userId,
      address,
      location,
      timeZone,
      userName,
      codeEmail,
      codePhone,
      venueName,
    } = data;

    const user = await userHelper.findOrCreateUser({
      email,
      phone,
      userId,
      timeZone,
      codeEmail,
      codePhone,
      name: userName,
      collection: db.MERCHANT_USERS
    });

    const venue = await venueHelper.findOrCreateVenue({
      role,
      extId,
      address,
      location,
      name: venueName,
      userId: user!.id
    });

    if (!venue || !user) {
      throw new functions.https.HttpsError("internal", INTERNAL_SERVER_ERROR);
    }

    const {
      users,
      admins,
      owners,
      allUserIds
    } = venue;

    const claimVenueDTO: IClaimVenueDTO = {
      role,
      status: EConfirmation.PENDING,
      createdAt: new Date().valueOf(),
    };

    await claimVenueHelper.checkClaimVenueExists({
      userId: user.id,
      venueId: venue.id,
    });

    claimVenueDTO.userId = user.id;
    claimVenueDTO.email = user.email;
    claimVenueDTO.phone = user.phone;
    claimVenueDTO.username = user.name;
    claimVenueDTO.venueId = venue.id;
    claimVenueDTO.qrCode = venue.qrCode;
    claimVenueDTO.location = venue.location;
    claimVenueDTO.latitude = venue.location.latitude;
    claimVenueDTO.longitude = venue.location.longitude;

    claimVenueHelper.claimVenueLogs({
      role,
      userId: user.id,
      venueId: venue.id
    });

    claimVenueHelper.sendClaimVenueMessage(claimVenueDTO);

    admin
      .firestore()
      .collection(db.CLAIM_VENUES)
      .add(claimVenueDTO);

    return { venueQrCode: claimVenueDTO.qrCode };
  }
}

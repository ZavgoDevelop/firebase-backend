// Sample code

export class BellDashboardController {
  static async createDashboardNotification(data: ICreateDashboardNotification) {
    const { type, userId, targetUserId, venueId } = data;

    const receivers: Array<IReceiver> = [];

    const venue = await VenuesController.getVenue({ venueId });

    const executorUser = await userHelper.findUser({ userId, collection: db.MERCHANT_USERS });
    const targetUser = await userHelper.findUser({ userId: targetUserId, collection: db.MERCHANT_USERS });

    if (ownerNotificationTypes.includes(<ENotificationType> type)) {
      const { owners, admins } = venue;

      const users = [
        ...(Array.isArray(owners) ? owners : []),
        ...(Array.isArray(admins) ? admins : []),
      ];

      users.map((item) =>
        (item.id !== userId && item.claimStatus === EOwnerStatus.APPROVED) &&
        receivers.push({ userId: item.id, received: false }));
    }

    if (userNotificationsTypes.includes(<ENotificationType> type) && targetUserId) {
      receivers.push({ userId: targetUserId, received: false });
    }

    const receiverIds = receivers.map((item) => item.userId);

    const targetVenueOmit = omit({ obj: venue, props: venuePrivateFields });
    const targetUserOmit = omit({ obj: targetUser, props: userPrivateFields });
    const executorUserOmit = omit({ obj: executorUser, props: userPrivateFields });

    const notificationDocument = await admin.firestore().collection(db.BELL_NOTIFICATIONS).doc();

    const notificationDTO: IDashboardNotificationDTO = {
      ...data,
      type,
      venueId,
      receivers,
      receiverIds,
      targetVenue: targetVenueOmit,
      executorUser: executorUserOmit,
      createdAt: new Date().valueOf(),
      notificationId: notificationDocument.id,
      targetUser: targetUserId ? targetUserOmit : executorUserOmit,
    };

    await notificationDocument.set(notificationDTO);

    return { success: true };
  }
}



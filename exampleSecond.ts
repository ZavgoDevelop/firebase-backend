// Firebase storage

import stream from "stream";
import admin from "firebase-admin";
import * as functions from "firebase-functions";

import { IUploadIcon } from "";
import { INTERNAL_SERVER_ERROR, NOT_FOUND } from "";

export const uploadFileInFirebaseBucket = async (data: IUploadIcon) => {
  const { file, id, prefix, collection } = data;
  const { originalname, buffer, mimetype } = file!;

  const essence = await admin.firestore().collection(collection).doc(id).get();

  if (!essence) {
    throw new functions.https.HttpsError("not-found", NOT_FOUND);
  }

  const bufferStream = new stream.PassThrough();
  bufferStream.end(buffer);

  const bucket = admin.storage().bucket();

  await bucket.deleteFiles({ prefix: `${prefix}/${id}` });

  const fileBucket = await bucket.file(`${prefix}/${id}/${originalname}`);
  const iconUrl = await fileBucket.getSignedUrl({ action: "read", expires: "03-01-2500" });

  await bufferStream.pipe(fileBucket.createWriteStream({ metadata: { contentType: mimetype } }))
    .on("error", error => ( console.error("FIREBASE BUCKET ERROR:", error)));

  if (!iconUrl) {
    throw new functions.https.HttpsError("internal", INTERNAL_SERVER_ERROR);
  }

  await essence.ref.set({ iconUrl: iconUrl[0] }, { merge: true });

  return { iconUrl: iconUrl[0] };
};

